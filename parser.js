const fs = require('fs');
const ASN1 = require('@lapo/asn1js');

const moduleParsers = [];

moduleParsers[2] = [];
moduleParsers[0x80] = [];

let pieces0205 = [];

moduleParsers[2][4] = payload => {
  // 0000036f3082
  var start = 0;
  const status = payload.readUInt16BE(start);
  start += 2;
  const tags = [];
  do {
    const length = payload.readUInt16BE(start);
    start += 2;
    if (start + length > payload.length) {
      console.log('length goes beyond end of buffer');
      console.log(payload.slice(start - 2).toString('hex'));
      break;
    }
    const content = payload.slice(start, start + length);
    start += length;

    const tag = ASN1.decode(content);
    tags.push(tag.toHexString());
  } while (start < payload.length);
  return {tags};
};

moduleParsers[2][5] = payload => {
  //000400 05ae
  if (payload.length === 2) {
    // parsing response
    const status = payload.readUInt16BE();
    return {status};
  }
  const index = payload[0];
  const piece_length = payload.readUInt16BE(1);
  const piece = payload.slice(3, 3 + piece_length);
  pieces0205[index] = piece;
  const full = Buffer.concat([...pieces0205]);

  const length = full.readUInt16BE(0);
  if (full.length - 2 < length) {
    return {index, length, complete: false};
  }
  const content = full.slice(2);
  pieces0205 = [];

  const ascii = content.slice(0, 0x60);
  const misc = content.slice(0x60, 0x260);
  // 2 bytes are the same, but don't seem to be part of the cert

  const cert = ASN1.decode(content.slice(0x262));
  return {
    index,
    length,
    complete: true,
    ascii: ascii.toString('ascii'),
    misc: misc.toString('hex'),
    cert: cert.toHexString(),
  };
};

let bin_length = 0;

moduleParsers[2][8] = payload => {
  if (payload.length === 2) {
    // parsing response
    const status = payload.readUInt16BE();
    return {status};
  }
  bin_length = payload.readUInt32BE(payload.length - 4);
  const other_length = payload.readUInt32BE(payload.length - 8);
  const diff = other_length - bin_length;

  const length = payload.readUInt32LE(payload.length - 12);
  const content = payload.slice(2, 2 + length); //?

  return {
    length,
    plength: payload.length,
    content: content.toString('hex'),
    other_length,
    bin_length,
    diff,
  };
};

const bin_pieces = [];

moduleParsers[2][9] = payload => {
  if (payload.length === 2) {
    // parsing response
    const status = payload.readUInt16BE();
    return {status};
  }

  const index = payload[0];
  const length = payload.readUInt16BE(1);
  const content = payload.slice(3, 3 + length);
  bin_pieces[index] = content;
  const bin = Buffer.concat([...bin_pieces]);
  if (bin.length >= bin_length) {
    return {index, length, complete: true, bin: bin.toString('hex')};
  }

  return {index, length, complete: false};
};

moduleParsers[0x80][1] = payload => {
  const status = payload.readUInt16BE();
  return {status};
};

function unframe(data) {
  if (data[0] !== 0x02 || data[data.length - 1] !== 0x03) {
    return data;
  }

  return data.slice(1, data.length - 3); // 1 byte STX, 3 byte CRC16 + ETX
}

function main(filename) {
  const content = fs.readFileSync(filename, 'utf-8');
  const lines = content.split('\n');

  lines.forEach((line, index) => {
    if (line.length === 0) {
      return;
    }

    let protected = false;
    const msg = unframe(Buffer.from(line, 'hex'));

    const header = msg.slice(0, 3);
    const sequenceNumber = header[2];
    let length = header.readUInt16BE(0);
    if (length > 0x8000) {
      length -= 0x8000;
      length += 5;
      protected = true;
    }
    if (msg.length - header.length < length) {
      console.log(
        sequenceNumber,
        `**************incomplete (${msg.length - header.length} < ${length})`,
        msg.toString('hex'),
      );
      return;
    }

    if (msg.length - header.length > length) {
      console.log(index, `${msg.length - header.length - length} excess bytes`);
    }

    const payload = msg.slice(3, 3 + length);

    if (protected) {
      const randomServerNumber = payload[0];
      const encrypted = payload.slice(1, payload.length - 4);
      const suffix = payload.slice(payload.length - 4);

      console.log({
        sequenceNumber,
        length: encrypted.length,
        randomServerNumber,
        encrypted: encrypted.toString('hex'),
        suffix: suffix.toString('hex'),
      });
    } else {
      const moduleId = payload[0];
      const commandId = payload[1];
      const parameters = payload.slice(2);

      const moduleParser = moduleParsers[moduleId];
      const commandParser = moduleParser && moduleParser[commandId];
      if (commandParser && parameters.length > 0) {
        try {
          const parsed = commandParser(parameters);
          if (moduleId === 0x80 && parsed.status === 0) {
            return; // Ignore protocol status
          }
          if (parsed.complete !== false) {
            console.log({
              sequenceNumber,
              moduleId,
              commandId,
              parsed,
            });
          }
        } catch (e) {
          console.log({
            sequenceNumber,
            moduleId,
            commandId,
            e,
            parameters: parameters.toString('hex'),
          });
        }
      } else if (parameters.length > 0) {
        console.log({
          sequenceNumber,
          moduleId,
          commandId,
          parameters: parameters.toString('hex'),
          parametersStr: parameters.toString('ascii'),
        });
      } else {
        console.log({
          sequenceNumber,
          moduleId,
          commandId,
        });
      }
    }
  });
}

if (process.argv.length < 3) {
  console.log('node parser.js filename');
} else {
  const filename = process.argv[2];
  main(filename);
}
