const fs = require('fs');
const EventEmitter = require('events');
const noble = require('@abandonware/noble');
const SerialPort = require('serialport');
const ASN1 = require('@lapo/asn1js');
const {crc16ccitt} = require('crc');

const HEX = 0x10;

const stx = 0x02;
const etx = 0x03;
//NACK_STATUS = 0xFFF8; //aka BADMSG

const modules = {
  unknown: 0x01,
  protocol: 0x80,
};
const commands = {
  info: 0x01,
  echo: 0xfe,
};

const tagNames = [];
tagNames[0x1] = 'SvppSoftwareVersion';
tagNames[0x2] = 'EmvL1KernelVersion';
tagNames[0x3] = 'EmvL2KernelVersion';
tagNames[0x4] = 'EmvCfg';
tagNames[0x5] = 'AtmelSerialNumber';
tagNames[0x6] = 'PartNumber(string)';
tagNames[0x7] = 'TerminalSerialNumber';
tagNames[0x8] = 'DisplayTextVersion';
tagNames[0x9] = 'TrxState';
tagNames[0xa] = 'MaxPayloadSize';
tagNames[0xb] = 'LinkMduleState';
tagNames[0xc] = 'BatteryState';
tagNames[0xd] = 'AutomaticPowerOffTimeout';
tagNames[0xe] = 'BaseFirmwareVersion';

class Device {
  constructor(mode = 'usb') {
    this.emitter = new EventEmitter();
    this.mode = mode;
    // TODO: Add auto that checks for serial port
    if (mode === 'ble') {
      noble.on('stateChange', async state => {
        if (state === 'poweredOn') {
          await noble.startScanningAsync([sumUpServiceUuid]);
        }
      });

      noble.on('discover', (data, isNotification) => {
        this.onPeripheral(data, isNotification);
      });
    } else if (mode === 'usb') {
      if (fs.existsSync('/dev/cu.usbmodemA1')) {
        this.serialport = new SerialPort('/dev/cu.usbmodemA1');
        this.serialport.on('data', data => {
          this.incoming(data, true);
        });
      } else {
        throw 'device not plugged in';
      }
    }
    this.incomplete = Buffer.alloc(0);
  }

  async onPeripheral(peripheral) {
    const sumUpServiceUuid = 'd839fc3c84dd4c369126187b07255129';
    const readCharUuid = '1f6b14c997fa4f1eaaa67e152fdd04f4';
    const writeCharUuid = 'b378db854ec34daa828e1b99607bd6a0';
    const wakeCharUuid = 'f953144be33a4079b202e3d7c1f3dbb0';
    const powerCharUuid = '22ffc5471bef48e2aa87b87e23ac0bbd';

    const {advertisement, uuid} = peripheral;
    console.log(advertisement);
    await noble.stopScanningAsync();
    await peripheral.connectAsync();
    console.log('connected');

    const {
      characteristics,
    } = await peripheral.discoverSomeServicesAndCharacteristicsAsync([
      sumUpServiceUuid,
    ]);
    characteristics.forEach(async characteristic => {
      const {uuid, properties} = characteristic;
      if (uuid === readCharUuid) {
        characteristic.on('data', incoming);
        characteristic.subscribe();
        await characteristic.readAsync();
        this.readChar = characteristic;
      } else if (uuid === writeCharUuid) {
        this.outgoing = characteristic;
      } else if (uuid == powerCharUuid) {
        characteristic.on('data', data => this.powerUpdate(data));
        characteristic.subscribe();
      } else if (uuid == wakeCharUuid) {
        this.wake = characteristic;
      }
    });

    if (!this.outgoing) {
      console.log('missing characteristics');
      return;
    }

    console.log('ready');
    setTimeout(() => {
      getDeviceInfo();
      //echo('hello');
    }, 1000);

    //await peripheral.disconnectAsync();
  }

  incoming(data, isNotification) {
    if (!isNotification) {
      console.log('Experiment: discard non-notification', data.toString('hex'));
      return;
    }
    console.log('incoming', data.toString('hex'));

    let message = this.extractCompleteMessage(data);
    while (message.complete) {
      const {sequenceNumber, payload} = message;
      const module = payload[0];
      const command = payload[1];
      const status = payload.readInt16BE(2);

      if (module == modules.protocol) {
        console.log('protocolStatus', status);
      } else {
        this.emitter.emit('message', {
          module,
          command,
          status,
          sequenceNumber,
          payload: payload.slice(4),
        });
      }

      //Check for more (ex: multiple frames in single buffer)
      message = this.extractCompleteMessage();
    }
  }

  extractCompleteMessage(data = Buffer.alloc(0)) {
    const msg = Buffer.concat([this.incomplete, data]);

    if (msg.length < 3) {
      //console.log('too short, retaining');
      this.incomplete = msg;
      return {complete: false};
    }

    //02 0004 00 8001 fff8 8141 03
    //02: start frame
    //0004: length
    //00: sequence number
    // ...
    //8141 crc16ccitt
    //03: end frame

    if (msg[0] !== stx) {
      //console.log('first byte is not start of frame', msg.toString('hex'));
      return {complete: false};
    }

    const header = msg.slice(1, 4);
    const length = header.readUInt16BE(0);
    // 4: start of frame byte + 2 byte length + 1 byte sequence after which payload starts
    // 3: 2 byte crc + 1 byte end of frame
    if (msg.length < 4 + length + 3) {
      //console.log(`this.incomplete (${msg.length} < ${length + 4}), retaining`);
      this.incomplete = msg;
      return {complete: false};
    }

    const sequenceNumber = header[2];
    const payload = msg.slice(4, 4 + length);
    if (msg[4 + length + 2] === etx) {
      this.incomplete = msg.slice(4 + length + 2 + 1);
      //console.log(`DEBUG: saving ${this.incomplete.length} bytes from end of data`);
    } else {
      console.log('last byte is not end of frame');
      // TODO: figure out what to do
    }

    const calcCrc = msg.readUInt16BE(1 + 3 + length);
    const crc = crc16ccitt(msg.slice(1, 4 + length), 0);

    if (crc !== calcCrc) {
      console.log('crc mismatch', crc, calcCrc);
      return {complete: false};
    }

    return {complete: true, sequenceNumber, payload};
  }

  powerUpdate(data, isNotification) {
    console.log(`power ${data[0]}%`);
  }
}

class SumUpPinPlus extends Device {
  constructor(mode) {
    super(mode);
    this.sequenceNumber = 1;
  }

  async echo(value) {
    const echoCmd = Buffer.alloc(2 + 3 + 2 + value.length);
    echoCmd.writeUInt16BE(value.length + 4, 0);
    echoCmd[2] = 0xff; // Sequence number, replaced in framed()
    echoCmd[3] = 0x01;
    echoCmd[4] = 0xfe;
    echoCmd.writeUInt16BE(value.length, 5);
    Buffer.from(value).copy(echoCmd, 7);

    const frame = this.framed(echoCmd);
    return this.sendData(frame);
  }

  //0011000101C1C2C3C4C5C6C7C8C9CACBCCCDCECF
  async getDeviceInfo() {
    const value = Buffer.from('C1C2C3C4C5C6C7C8C9CACBCCCDCECF', 'hex');
    //const value = Buffer.from('C1', 'hex');
    const getInfo = Buffer.alloc(3 + 2 + value.length);
    getInfo.writeUInt16BE(value.length + 2, 0);
    getInfo[2] = 0xff; // Sequence number, replaced in framed()
    getInfo[3] = 0x01;
    getInfo[4] = 0x01;
    value.copy(getInfo, 5);

    const frame = this.framed(getInfo);
    return this.sendData(frame).then(this.parseDeviceInfo.bind(this));
  }

  parseDeviceInfo({payload}) {
    const tags = [];
    let start = 0;
    do {
      const tag = ASN1.decode(payload.slice(start));
      tags.push(tag);
      start += tag.header + tag.length;
    } while (start < payload.length);

    tags.forEach(tag => {
      const content = tag.content().replace('\n', ' ');
      const {tagNumber} = tag.tag;
      let name = '';
      if (tagNames[tagNumber]) {
        name = tagNames[tagNumber];
      }

      console.log(name || tagNumber, content);
    });
  }

  //PINPLUS_DEVICE_POWER_OFF_COMMAND = 'AAIBAQ4='; //0002 01 010e
  async powerOff() {
    const cmd = Buffer.alloc(3 + 2);
    cmd.writeUInt16BE(2, 0);
    cmd[2] = 0xff; // Sequence number, replaced in framed()
    cmd[3] = 0x01;
    cmd[4] = 0x0e;
    const frame = this.framed(cmd);
    return this.sendData(frame);
  }

  //PINPLUS_SHOW_DEFAULT_MESSAGE = 'ABUBAQsAAAABAAtTdW1VcCBQSU4rAP8A';
  //00150101 0b 0000 0001 000b 53756d55702050494e2b00 ff00
  async showDefaultMessage(message = 'SumUp PIN+') {
    const msgBuffer = Buffer.from(message);
    const prefix = Buffer.from('00000001000b', 'hex');
    prefix.writeUInt16BE(msgBuffer.length + 1, 4); // + 1 : null terminator in the suffix
    const suffix = Buffer.from('00ff00', 'hex');
    const value = Buffer.concat([prefix, msgBuffer, suffix]);
    const cmd = Buffer.alloc(3 + 2 + value.length);
    cmd.writeUInt16BE(2 + value.length, 0);
    cmd[2] = 0xff; // Sequence number, replaced in framed()
    cmd[3] = 0x01;
    cmd[4] = 0x0b;
    value.copy(cmd, 5);
    const frame = this.framed(cmd);
    return this.sendData(frame);
  }

  //0002060201
  async enterProtectedMode() {
    const cmd = Buffer.alloc(3 + 2);
    cmd.writeUInt16BE(2, 0);
    cmd[2] = 0xff; // Sequence number, replaced in framed()
    cmd[3] = 0x02;
    cmd[4] = 0x01;
    const frame = this.framed(cmd);
    return this.sendData(frame);
  }

  async getCerts() {
    const cmd = Buffer.alloc(3 + 2);
    cmd.writeUInt16BE(2, 0);
    cmd[2] = 0xff; // Sequence number, replaced in framed()
    cmd[3] = 0x02;
    cmd[4] = 0x04;
    const frame = this.framed(cmd);
    return this.sendData(frame).then(({payload}) => {
      var start = 0;
      const tags = [];
      do {
        const length = payload.readUInt16BE(start);
        start += 2;
        if (start + length > payload.length) {
          console.log('length goes beyond end of buffer');
          console.log(payload.slice(start - 2).toString('hex'));
          break;
        }
        const content = payload.slice(start, start + length);
        start += length;

        const tag = ASN1.decode(content);
        console.log(tag.toPrettyString());
        tags.push(tag.toHexString());
      } while (start < payload.length);
      return {tags};
    });
  }

  framed(command, protect = false) {
    const frame = Buffer.alloc(command.length + 4);
    const length = command.readUInt16BE(0);
    command[2] = this.sequenceNumber++ % 0xff;
    frame[0] = 2;
    command.copy(frame, 1);

    const crc = crc16ccitt(command, 0);
    frame.writeUInt16BE(crc, 1 + command.length);
    frame[command.length + 3] = 3;

    console.log('framed command', frame.toString('hex'));
    return frame;
  }

  async sendData(frame) {
    const MAX_CHARACTERISTIC_SIZE = 20;
    if (this.mode === 'usb') {
      await this.serialport.write(frame);
    } else if (this.mode === 'ble') {
      const length = frame.length;
      let start = 0;

      do {
        const chunkSize = Math.min(MAX_CHARACTERISTIC_SIZE, length - start);
        await outgoing.writeAsync(frame.slice(start, start + chunkSize), true);
        console.log(`sent ${chunkSize} bytes`);
        start += chunkSize;
      } while (start < length);
    }

    return new Promise((resolve, reject) => {
      const self = this;
      this.emitter.on('message', function waitForAck(message) {
        const {sequenceNumber, status} = message;
        if (sequenceNumber === frame[3]) {
          self.emitter.off('message', waitForAck);
          if (status === 0) {
            resolve(message);
          } else {
            reject(message);
          }
        }
      });
    });
  }
}

async function main() {
  console.log('start');
  const pinplus = new SumUpPinPlus();
  //await pinplus.getDeviceInfo();
  console.log(await pinplus.getCerts());
  //await pinplus.showDefaultMessage('feed me a stray cat');
  //await pinplus.enterProtectedMode();

  return true;
}

main()
  .then(console.log)
  .catch(console.log);
